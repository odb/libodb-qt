/* file      : odb/qt/details/build2/config-vc.h
 * license   : GNU GPL v2; see accompanying LICENSE file
 */

/* Configuration file for Windows/VC++ for the build2 build. */

#ifndef ODB_QT_DETAILS_CONFIG_VC_H
#define ODB_QT_DETAILS_CONFIG_VC_H

/* Define LIBODB_QT_BUILD2 for the installed case. */
#ifndef LIBODB_QT_BUILD2
#  define LIBODB_QT_BUILD2
#endif

#endif /* ODB_QT_DETAILS_CONFIG_VC_H */
